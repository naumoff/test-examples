#fu3e test:

Please run those commands to test csv application:
```
php artisan storage:link
php artisan:migrate
npm install && npm run watch
```

To run tests:
```
phpunit tests
```

** P.S. I am not front-end developer and have not touched js, and vue.js almost one year, but eager to refresh this stack and use it in conjunction with Laravel.**
