<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CsvFile extends Model
{
    protected $table = 'csv_files';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    /**
     * @param array $data
     * @return $this
     */
    public function setCsv(array $data): self
    {
        $csvHandle = $csv = fopen('php://temp/maxmemory:'. (5 * 1024 * 1024), 'r+');
        foreach ($data as $key => $row) {
            if ($key === 0) {
                fputcsv($csvHandle, array_keys($row));
            }
            fputcsv($csvHandle, $row);
        }
        rewind($csvHandle);
        $this->csv = stream_get_contents($csvHandle);
        return $this;
    }
}
