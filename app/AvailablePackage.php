<?php

namespace App;

use App\Collections\AvailablePackageCollection;
use Illuminate\Database\Eloquent\Model;

class AvailablePackage extends Model
{
    protected $table = 'available_packages';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function newCollection(array $models = []): AvailablePackageCollection
    {
        return new AvailablePackageCollection($models);
    }
}
