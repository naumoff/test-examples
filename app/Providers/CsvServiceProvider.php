<?php

namespace App\Providers;

use App\Services\Contracts\CsvStorageContract;
use App\Services\CsvStorageService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class CsvServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(CsvStorageContract::class, CsvStorageService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * @return string[]
     */
    public function provides(): array
    {
        return [CsvStorageContract::class];
    }
}
