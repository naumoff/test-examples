<?php

namespace App\Providers;

use App\Services\Contracts\PackageCompilerContract;
use App\Services\PackageCompilerService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(PackageCompilerContract::class, PackageCompilerService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * @return string[]
     */
    public function provides(): array
    {
        return [PackageCompilerContract::class];
    }
}
