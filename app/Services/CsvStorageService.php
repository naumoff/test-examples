<?php

namespace App\Services;

use App\CsvFile;
use App\Services\Contracts\CsvStorageContract;
use Illuminate\Support\Facades\Storage;

class CsvStorageService implements CsvStorageContract
{
    public function store(array $data): CsvFile
    {
        $csvFile = new CsvFile();
        $csvFile->setCsv($data);
        $csvFile->save();

        $content = $csvFile->csv;
        $fileName = $csvFile->id . '.csv';

        Storage::disk('public')->put($fileName, $content);

        $fullPathToFile  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix() . $fileName;
        $csvFile->path_to_file = $fullPathToFile;
        $csvFile->url_to_file = asset('storage/' . $fileName);
        $csvFile->save();
        return $csvFile;
    }
}
