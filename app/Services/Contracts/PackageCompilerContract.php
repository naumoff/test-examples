<?php

namespace App\Services\Contracts;

use App\Services\PackageCompilerService;

/**
 * Interface PackageCompilerContract
 * @package App\Services\Contracts
 * @see PackageCompilerService::compile()
 */
interface PackageCompilerContract
{
    /**
     * @param int $qty
     * @param array $packages
     * @return array
     */
    public function compile(int $qty, array $packages): array;
}
