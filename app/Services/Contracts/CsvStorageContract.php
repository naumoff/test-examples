<?php

namespace App\Services\Contracts;

use App\CsvFile;
use App\Services\CsvStorageService;

/**
 * Interface CsvStorageContract
 * @package App\Services\Contracts
 * @see CsvStorageService::store()
 */
interface CsvStorageContract
{
    public function store(array $data): CsvFile;
}
