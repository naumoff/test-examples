<?php

namespace App\Services;

use App\Services\Contracts\PackageCompilerContract;

class PackageCompilerService implements PackageCompilerContract
{
    /**
     * @param int $qty
     * @param array $packages
     * @return array
     */
    public function compile(int $qty, array $packages): array
    {
        rsort($packages, SORT_NUMERIC);
        $qty = $this->setFinalOrderQty($qty, $packages);
        $compiledOrder = [];
        foreach ($packages as $package) {
            if ($qty === 0) {
                break;
            }

            if (($packQty = $qty / $package) >= 1) {
                $packQty = floor($packQty);
                $qty -= ($packQty * $package);
                $compiledOrder[$package] = (int) $packQty;
            }
        }
        return $compiledOrder;

    }

    /**
     * @param int $qty
     * @param array $packages
     * @return int
     */
    private function setFinalOrderQty(int $qty, array $packages): int
    {
        rsort($packages, SORT_NUMERIC); // just in case
        $compiledOrder = [];
        foreach ($packages as $index => $package) {
            if ($qty < 0 || $qty === 0) {
                break;
            }
            if (($packQty = $qty / $package) >= 1) {
                $packQty = floor($packQty);
                $qty -= ($packQty * $package);
                $compiledOrder[$package] = $packQty;
            }
        }

        if (count($compiledOrder) === 0 || $qty > 0) {
            $compiledOrder[$package] = (isset($compiledOrder[$package]))? ++$compiledOrder[$package] : 1;
        }

        $finalOrderQty = 0;
        foreach ($compiledOrder as $qtyInPackage => $packageQty) {
            $finalOrderQty += $qtyInPackage * $packageQty;
        }
        return $finalOrderQty;
    }
}
