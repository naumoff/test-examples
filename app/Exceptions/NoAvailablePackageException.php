<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class NoAvailablePackageException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        return new JsonResponse([
            'errors' => [
                "status" => 500,
                "title" => "No Package found!",
                "detail" => "Add packages before order compiling"
            ]
        ], 500);
    }
}
