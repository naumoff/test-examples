<?php

namespace App\Collections;

use App\AvailablePackage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class AvailablePackageCollection extends Collection
{
    /**
     * @return array
     */
    public function pluckQuantityArray(): array
    {
        return $this->pluck('pack_qty')->toArray();
    }

    public function collectionMassUpdate(array $requestArray)
    {
        DB::beginTransaction();
        try {
            $requestCollection = collect($requestArray['data']);
            /** @var AvailablePackage $package */
            foreach ($this as $package) {
                if (!in_array($package->id, data_get($requestArray, 'data.*.id'))) {
                    $package->delete();
                    continue;
                } else {
                    $input = $requestCollection->whereIn('id', $package->id)->first();
                    $package->pack_qty = $input['pack_qty'];
                    $package->save();
                    continue;
                }
            }
            foreach ($requestCollection as $request) {
                if (!isset($request['id'])) {
                    $package = new AvailablePackage();
                    $package->pack_qty = $request['pack_qty'];
                    $package->save();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }
}
