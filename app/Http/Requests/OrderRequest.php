<?php

namespace App\Http\Requests;

use App\AvailablePackage;
use App\Exceptions\NoAvailablePackageException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class OrderRequest extends FormRequest
{
    /** @var array */
    public $resolvedPackages;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "quantity" => "required|integer|min:1"
        ];
    }

    /**
     * @param Validator $validator
     * @throws NoAvailablePackageException
     */
    public function withValidator(Validator $validator): void
    {
        if(! $validator->fails()){
            $this->resolvedPackages = AvailablePackage::all()->pluckQuantityArray();
            if (empty($this->resolvedPackages)) {
                throw new NoAvailablePackageException();
            }
        }
    }
}
