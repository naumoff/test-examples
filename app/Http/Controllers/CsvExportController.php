<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CsvRequest;
use App\Http\Resources\CsvFileResource;
use App\Services\Contracts\CsvStorageContract;

class CsvExportController extends Controller
{

    /** @var CsvStorageContract */
    private $csvStorageService;

    public function __construct(CsvStorageContract $csvStorageService)
    {
        $this->csvStorageService = $csvStorageService;
    }

    /**
     * @param CsvRequest $request
     * @return CsvFileResource
     */
    public function convert(CsvRequest $request): CsvFileResource
    {
        $csvFile = $this->csvStorageService->store($request->validated());
        return new CsvFileResource($csvFile);
    }
}
