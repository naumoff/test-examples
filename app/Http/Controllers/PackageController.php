<?php

namespace App\Http\Controllers;

use App\AvailablePackage;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\UpdatePackagesRequest;
use App\Http\Resources\AvailablePackageResourse;
use App\Http\Resources\CompiledOrderResource;
use App\Services\Contracts\PackageCompilerContract;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PackageController extends Controller
{
    /** @var PackageCompilerContract */
    private $packageCompilerService;

    public function __construct(PackageCompilerContract $packageCompilerContract)
    {
        $this->packageCompilerService = $packageCompilerContract;
    }

    /**
     * @param OrderRequest $request
     * @return CompiledOrderResource
     */
    public function compile(OrderRequest $request): CompiledOrderResource
    {
        $order = $this->packageCompilerService->compile($request->get('quantity'), $request->resolvedPackages);
        return new CompiledOrderResource($order);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return AvailablePackageResourse::collection(AvailablePackage::all());
    }

    /**
     * @param UpdatePackagesRequest $request
     * @return AnonymousResourceCollection
     */
    public function update(UpdatePackagesRequest $request): AnonymousResourceCollection
    {
        AvailablePackage::all()->collectionMassUpdate($request->validated());
        return AvailablePackageResourse::collection(AvailablePackage::all());
    }
}
