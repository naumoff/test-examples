<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompiledOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [];
        $itemsQty = 0;
        foreach ($this->resource as $packageSet => $packageQty) {
            $itemsQty += $packageSet * $packageQty;
            $result[$packageSet] = $packageQty;
        }
        $result['total_items_qty'] = $itemsQty;
        return $result;
    }
}
