<?php

use App\AvailablePackage;
use Illuminate\Database\Seeder;

class AvailablePackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AvailablePackage::class)->create(['pack_qty' => 250]);
        factory(AvailablePackage::class)->create(['pack_qty' => 500]);
        factory(AvailablePackage::class)->create(['pack_qty' => 1000]);
        factory(AvailablePackage::class)->create(['pack_qty' => 2000]);
        factory(AvailablePackage::class)->create(['pack_qty' => 5000]);
    }
}
