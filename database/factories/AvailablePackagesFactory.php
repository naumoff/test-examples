<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AvailablePackage;
use Faker\Generator as Faker;

$factory->define(\App\AvailablePackage::class, function (Faker $faker) {
    return [
        'pack_qty' => $faker->numberBetween($min = 1, $max = 9000)
    ];
});
