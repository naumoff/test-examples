<?php

namespace Tests\Feature;

use App\CsvFile;
use App\Services\Contracts\CsvStorageContract;
use App\Services\CsvStorageService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CsvExportTest extends TestCase
{
    use RefreshDatabase;

    private const PUBLIC_DIRECTORY = __DIR__.'/../../storage/app/public';

    #region DATA PROVIDERS

    /**
     * @return array[]
     */
    public function csvCorrectArrayProvider(): array
    {
        $row1 = ['first_name' => 'Andrey', 'last_name' => 'Naumoff', 'email' => 'andrey.naumoff@gmail.com'];
        $row2 = ['first_name' => 'John', 'last_name' => 'Doe', 'email' => 'john.doe@gmail.com'];
        $structure = [
            'data' => [
                'id',
                'url',
                'created_at'
            ]
        ];
        return [
            [
                $row1, $row2, $structure
            ]
        ];
    }

    /**
     * @return array[]
     */
    public function csvWrongArrayProvider(): array
    {
        $row1 = ['first_name' => [], 'last_name' => 'Naumoff', 'email' => 'andrey.naumoff@gmail.com'];
        $row2 = ['first_name' => 'Andrey', 'last_name' => 'Naumoff', 'email' => 'andrey.naumoff@gmail.com'];
        $row3 = ['first_name' => false, 'last_name' => 'Doe', 'email' => 'john.doe@gmail.com'];
        $row4 = ['first_name' => 'John', 'last_name' => 'Doe', 'email' => 'john.doe@gmail.com'];
        $structure = [
            'message',
            'errors' => [
                "0.first_name"
            ]
        ];
        return [
            [
                $row1, $row2, $structure
            ],
            [
                $row3, $row4, $structure
            ]
        ];
    }
    #endregion

    #region TESTS
    /**
     * @test
     */
    public function app_is_working(): void
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function csv_storage_service_injected_correctly(): void
    {
        $storageService = app()->make(CsvStorageContract::class);
        $this->assertIsObject($storageService);
        $this->assertInstanceOf(CsvStorageContract::class, $storageService);
        $this->assertInstanceOf(CsvStorageService::class, $storageService);
    }

    /**
     * @test
     */
    public function public_storage_folder_exists(): void
    {
        $this->assertDirectoryExists(self::PUBLIC_DIRECTORY);
        $this->assertIsReadable(self::PUBLIC_DIRECTORY);
    }

    /**
     * @test
     * @param array $row1
     * @param array $row2
     * @param array $structure
     * @dataProvider csvCorrectArrayProvider
     */
    public function csv_export_endpoint_working(array $row1, array $row2, array $structure): void
    {
        /** @var TestResponse $response */
        $response = $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('POST', '/api/csv-export/', [$row1, $row2]);
        $response->assertStatus(201);
        $response->assertJsonStructure($structure);
        $response->assertJsonMissingValidationErrors();
        $this->assertDatabaseHas('csv_files', ['id' => 1]);
        $this->assertSame(1, CsvFile::all()->count());
        $file = self::PUBLIC_DIRECTORY . '/1.csv';
        $this->assertFileExists($file);
        $this->assertFileIsReadable($file);
        $fileContent = Storage::disk('public')->get('1.csv');
        $dbContent = CsvFile::first()->csv;
        $this->assertSame($fileContent, $dbContent);
    }

    /**
     * @test
     * @param array $row1
     * @param array $row2
     * @param array $structure
     * @dataProvider csvWrongArrayProvider
     */
    public function csv_export_endpoint_validate_wrong_data(array $row1, array $row2, array $structure): void
    {
        /** @var TestResponse $response */
        $response = $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('POST', '/api/csv-export/', [$row1, $row2]);
        $response->assertStatus(422);
        $response->assertJsonStructure($structure);
        $response->assertJsonPath('message', "The given data was invalid.");
        $response->assertJsonFragment([0 => 'The 0.first_name must be a string.']);
    }
    #endregion
}
