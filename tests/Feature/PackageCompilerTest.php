<?php

namespace Tests\Feature;

use App\AvailablePackage;
use App\Services\Contracts\PackageCompilerContract;
use App\Services\PackageCompilerService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;


class PackageCompilerTest extends TestCase
{
    use RefreshDatabase;

    private const ORDER_COMPILE_URI = '/api/order-compile/';
    private const PACKAGES_UPDATE_URI = '/api/packages/';

    /** @var PackageCompilerContract */
    private $packageCompilerService;

    #region SERVICE METHODS
    public function setUp(): void
    {
        parent::setUp();
        $this->packageCompilerService = app()->make(PackageCompilerContract::class);
    }

    private function seedAvailablePackagesTable(): void
    {
        Artisan::call('db:seed --class=AvailablePackagesTableSeeder');
    }

    /**
     * @param array $data
     * @return TestResponse
     */
    private function doOrderCompileRequest(array $data = []): TestResponse
    {
        return $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('GET', self::ORDER_COMPILE_URI, $data);
    }

    /**
     * @param array $data
     * @return TestResponse
     */
    private function doPackagesUpdateRequest(array $data = []): TestResponse
    {
        return $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('POST', self::PACKAGES_UPDATE_URI, $data);
    }
    #endregion

    #region DATA PROVIDERS
    public function correctCompilerResultsProvider(): array
    {
        $packageOne = [15, 30, 50, 25];
        $packageTwo = [500, 250, 5000, 1000, 2000];
        return [
            [1, $packageOne, [15 => 1]],
            [32, $packageOne, [30 => 1, 15 => 1]],
            [1, $packageTwo, [250 => 1]],
            [250, $packageTwo, [250 => 1]],
            [251, $packageTwo, [500 => 1]],
            [501, $packageTwo, [500 => 1, 250 => 1]],
            [12001, $packageTwo, [5000 => 2, 2000 => 1, 250 => 1]]
        ];
    }
    #endregion

    #region TESTS
    /**
     * @test
     */
    public function package_compiler_service_injected_correctly(): void
    {
        $this->assertIsObject($this->packageCompilerService);
        $this->assertInstanceOf(PackageCompilerContract::class, $this->packageCompilerService);
        $this->assertInstanceOf(PackageCompilerService::class, $this->packageCompilerService);
    }

    /**
     * @test
     * @param int $orderQty
     * @param array $packages
     * @param array $delivery
     * @dataProvider correctCompilerResultsProvider
     */
    public function package_compiler_service_working(int $orderQty, array $packages, array $delivery): void
    {
        self::assertSame($delivery, $this->packageCompilerService->compile($orderQty, $packages));
    }

    /**
     * @test
     */
    public function package_compiler_endpoint_validation_working(): void
    {
        $response = $this->doOrderCompileRequest(['quantity' => 'string']);
        $response->assertStatus(422);
        $response->assertJsonPath('errors.quantity.0', 'The quantity must be an integer.');

        $response = $this->doOrderCompileRequest();
        $response->assertStatus(422);
        $response->assertJsonPath('errors.quantity.0', 'The quantity field is required.');
    }

    /**
     * @test
     */
    public function db_seeder_works_correctly(): void
    {
        $this->seedAvailablePackagesTable();
        $this->assertSame(5, AvailablePackage::all()->count());
    }

    /**
     * @test
     */
    public function package_compiler_endpoint_working(): void
    {
        // request to empty db
        $response = $this->doOrderCompileRequest(['quantity' => 15]);
        $response->assertStatus(500);
        $response->assertJsonPath('errors.title', "No Package found!");

        // requests to seeded db
        $this->seedAvailablePackagesTable();
        $response = $this->doOrderCompileRequest(['quantity' => 15]);
        $response->assertStatus(200);
        $response->assertExactJson(['data' => [250 => 1, 'total_items_qty' => 250]]);

        $response = $this->doOrderCompileRequest(['quantity' => 12001]);
        $response->assertStatus(200);
        $response->assertExactJson(['data' => [5000 => 2, 2000 => 1, 250 => 1, 'total_items_qty' => 12250]]);
    }

    /**
     * @test
     */
    public function available_packages_list_endpoint_working(): void
    {
        // request to empty db
        $response = $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('GET', '/api/packages/');
        $response->assertStatus(200);
        $response->assertExactJson(['data' => []]);

        $this->seedAvailablePackagesTable();
        // request table with data
        $response = $this->withHeaders([
            "Accept"=> "application/json",
            "Content-Type" => "application/json"
        ])->json('GET', '/api/packages/');
        $response->assertStatus(200);
        $response->assertExactJson(['data' => [
            ['id' => 1, 'pack_qty' => '250'],
            ['id' => 2, 'pack_qty' => '500'],
            ['id' => 3, 'pack_qty' => '1000'],
            ['id' => 4, 'pack_qty' => '2000'],
            ['id' => 5, 'pack_qty' => '5000']
        ]]);
    }

    /**
     * @test
     */
    public function packages_post_endpoint_validation_working(): void
    {
        $response = $this->doPackagesUpdateRequest();
        $response->assertStatus(422);
        $response->assertJsonPath('errors.data.0', 'The data field is required.');

        $response = $this->doPackagesUpdateRequest(["data" => "string"]);
        $response->assertStatus(422);
        $response->assertJsonPath('errors.data.0', 'The data must be an array.');

        $response = $this->doPackagesUpdateRequest(
            [
                "data" => [
                    [
                        "id" => "string",
                        "pack_qty" => 0
                    ]
                ]
            ]
        );
        $response->assertStatus(422);
        $response->assertJsonFragment([
            "errors" => [
                "data.0.id" => ["The data.0.id must be an integer."],
                "data.0.pack_qty" => ["The data.0.pack_qty must be at least 1."],
            ]
        ]);

        $response = $this->doPackagesUpdateRequest(
            [
                "data" => [
                    [
                        "id" => 1,
                        "pack_qty" => 1500
                    ],
                    [
                        "pack_qty" => 1500
                    ]
                ]
            ]
        );
        $response->assertStatus(422);
        $response->assertJsonFragment([
            "errors" => [
                "data.0.id" => ["The selected data.0.id is invalid."],
                "data.0.pack_qty" => ["The data.0.pack_qty field has a duplicate value."],
                "data.1.pack_qty" => ["The data.1.pack_qty field has a duplicate value."],
            ]
        ]);
    }

    /**
     * @test
     */
    public function packages_post_endpoint_working(): void
    {
        $this->seedAvailablePackagesTable();
        $response = $this->doPackagesUpdateRequest(
            [
                "data" => [
                    [
                        "id" => 1,
                        "pack_qty" => 990
                    ],
                    [
                        "pack_qty" => 1500
                    ]
                ]
            ]
        );
        $response->assertStatus(200);
        $this->assertSame(2, AvailablePackage::all()->count());
        $package = AvailablePackage::where('pack_qty', '=', 990)->first();
        $this->assertSame(1, $package->id);
        $package = AvailablePackage::where('pack_qty', '=', 1500)->first();
        $this->assertSame(6, $package->id);
    }
    #endregion
}
